# -*- coding: utf-8 -*-
import subprocess
import sys
import uuid
import platform

name = "test_name"
namespace = "test_namespace"

aUUID = str(uuid.uuid1())
#print aUUID
#print aUUID[0]
sysstr = platform.system()

def toHex(s):
    lst = []
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        if len(hv) == 1:
            hv = '0'+hv
        lst.append(hv)
    return reduce(lambda x, y:x+y, lst)

def getUUID():
	if sysstr == "Windows":
		return str(uuid.uuid4())
	elif sysstr == "Darwin":
		p=subprocess.Popen("uuidgen", shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		(stdoutput,erroutput) = p.communicate()
		return stdoutput
	else:
		return str(uuid.uuid1())

#print uuid.uuid3(namespace, name)
#print uuid.uuid3(uuid.NAMESPACE_DNS, 'python.org')
#print uuid.uuid4()
#print uuid.uuid5(namespace, name)
#print uuid.uuid()
#x = uuid.UUID('{00010203-0405-0607-0809-0a0b0c0d0e0f}')
#print x.bytes
#check_output("ls -l .", shell=True);
argvslen = len(sys.argv)
bUUID = ""
if argvslen == 1:
    bUUID = getUUID()
else :
    strNumber = sys.argv[1]
    if len(sys.argv[1]) > 1:
        print "We just need only ONE char!!"
        strNumber = sys.argv[1][0]
    if strNumber == 'a' or strNumber == 'A':
        strNumber = 'A'
    elif strNumber == 'b' or strNumber == 'B':
        strNumber = 'B'
    elif strNumber == 'c' or strNumber == 'C':
        strNumber = 'C'
    elif strNumber == 'd' or strNumber == 'D':
        strNumber = 'D'
    elif strNumber == 'e' or strNumber == 'E':
        strNumber = 'E'
    elif strNumber == 'f' or strNumber == 'F':
        strNumber = 'F'
    elif strNumber >= '0' and strNumber <= '9':
        strNumber = strNumber
    else:
        print "Invalid number, so just 0"
        strNumber = '0'

    while True:
        uuidGet = getUUID()
        if uuidGet[0] == strNumber or uuidGet[0] == strNumber.lower():
            bUUID = uuidGet
            print "Got it!"
            break
        else:
            print uuidGet
print bUUID

#6315C697-AFFC-4D6B-A0C2-BFCEC10019B1
def toHex1(bUUID):
    aList = []
    i = 0
    aList.append(bUUID[0]+bUUID[1])
    aList.append(bUUID[2]+bUUID[3])
    aList.append(bUUID[4]+bUUID[5])
    aList.append(bUUID[6]+bUUID[7])

    aList.append(bUUID[9]+bUUID[10])
    aList.append(bUUID[11]+bUUID[12])

    aList.append(bUUID[14]+bUUID[15])
    aList.append(bUUID[16]+bUUID[17])

    aList.append(bUUID[19]+bUUID[20])
    aList.append(bUUID[21]+bUUID[22])

    aList.append(bUUID[24]+bUUID[25])
    aList.append(bUUID[26]+bUUID[27])
    aList.append(bUUID[28]+bUUID[29])
    aList.append(bUUID[30]+bUUID[31])
    aList.append(bUUID[32]+bUUID[33])
    aList.append(bUUID[34]+bUUID[35])

    aList.reverse()
    for strHex in aList:
        print "0x%s," % strHex,
    print ""

#6315C697-AFFC-4D6B-A0C2-BFCEC10019B1
def toHex2(bUUID):
    mo = True
    for i in range(0, len(bUUID)-1):
        if bUUID[i] == "-":
            mo = not mo;
            continue
        if mo == True:
            if (i % 2) != 0:
                continue
        else:
            if (i % 2) == 0:
                continue
        str = bUUID[i] + bUUID[i+1];
        print "0x%s," % str,

toHex1(bUUID)
toHex2(bUUID)
